package cz.cvut.fel.manilvit.model.items;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ReciepeTest {

    @Test
    void testTryCraft_NullItemNames() {
        List<String> items = new ArrayList<>();
        Reciepe reciepe = new Reciepe(items, new Bag());
        assertNull(reciepe.tryCraft(null));
    }

    @Test
    void testTryCraft_NotEnoughIngredients() {
        List<String> itemsToCraft = new ArrayList<>();
        itemsToCraft.add("BagPart1");
        itemsToCraft.add("BagPart2");
        Reciepe reciepe = new Reciepe(itemsToCraft, new Bag());

        List<String> itemsInCraft = new ArrayList<>();
        itemsInCraft.add("BagPart1");

        assertNull(reciepe.tryCraft(itemsInCraft));
    }

    @Test
    void testTryCraft_EnoughIngredients() {
        List<String> itemsToCraft = new ArrayList<>();
        itemsToCraft.add("BagPart1");
        itemsToCraft.add("BagPart2");
        Item bag= new Bag();
        Reciepe reciepe = new Reciepe(itemsToCraft, bag);

        List<String> itemsInCraft = new ArrayList<>();
        itemsInCraft.add("BagPart1");
        itemsInCraft.add("BagPart2");
        assertEquals(bag, reciepe.tryCraft(itemsInCraft));
    }

    @Test
    void testTryCraft_ExtraIngredients() {
        List<String> itemsToCraft = new ArrayList<>();
        itemsToCraft.add("BagPart1");
        itemsToCraft.add("BagPart2");
        Item bag= new Bag();
        Reciepe reciepe = new Reciepe(itemsToCraft, bag);

        List<String> itemsInCraft = new ArrayList<>();
        itemsInCraft.add("BagPart1");
        itemsInCraft.add("BagPart2");
        itemsInCraft.add("KeyPart1");

        assertNull(reciepe.tryCraft(itemsInCraft));

    }

}