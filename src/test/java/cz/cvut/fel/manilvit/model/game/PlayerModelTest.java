package cz.cvut.fel.manilvit.model.game;

import cz.cvut.fel.manilvit.model.items.Boots;
import cz.cvut.fel.manilvit.model.items.GameEnd;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class PlayerModelTest {
    

    @Test
    void testPickUpItem_EndTheGame() {
        PlayerModel playerModel = new PlayerModel();
        playerModel.setInventoryModel(new InventoryModel(3));
        assertTrue(playerModel.pickUpItem(new GameEnd()));
        assertTrue(playerModel.isEndTheGame());
    }

    @Test
    void testPickUpItem_SpeedBoost() {
        PlayerModel playerModel = new PlayerModel();
        playerModel.setInventoryModel(new InventoryModel(3));
        int speedBefore = playerModel.getSpeed();
        Boots boots = new Boots();
        assertTrue(playerModel.pickUpItem(boots));
        int speedAfter = playerModel.getSpeed();
        assertEquals(speedBefore, speedAfter - boots.getSpeedBoost());
    }



}