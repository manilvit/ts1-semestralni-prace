package cz.cvut.fel.manilvit.model.game;

import cz.cvut.fel.manilvit.model.items.BagPart1;
import cz.cvut.fel.manilvit.model.items.BagPart2;
import cz.cvut.fel.manilvit.model.items.Item;
import cz.cvut.fel.manilvit.model.items.KeyPart1;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryModelTest {
    @Test
    void testFindIndexForCraftedItem_EmptyCraftAndEmptyInventory() {
        InventoryModel inventory = new InventoryModel(3);
        assertEquals(-1, inventory.findIndexForCraftedItem());
    }

    @Test
    void testFindIndexForCraftedItem_EmptyCraftAndNotEmptyInventory() {
        InventoryModel inventoryModel = new InventoryModel(3);
        inventoryModel.addItem(new BagPart1());
        inventoryModel.addItem(new BagPart2());
        assertEquals(-1, inventoryModel.findIndexForCraftedItem());
    }

    @Test
    void testFindIndexForCraftedItem_FondIndex() {
        InventoryModel inventoryModel = new InventoryModel(3);

        Item keyPart1 = new KeyPart1();

        Item bagPart1 = new BagPart1();
        bagPart1.setAddedToCraft(true);

        Item bagPart2 = new BagPart2();
        bagPart2.setAddedToCraft(true);

        inventoryModel.addItem(keyPart1);
        inventoryModel.addItem(bagPart1);
        inventoryModel.addItem(bagPart2);


        assertEquals(2, inventoryModel.findIndexForCraftedItem());
    }


    @Test
    void testAddItem_EmptyInventory() {
        InventoryModel inventory = new InventoryModel(3);
        Item item = new BagPart1();
        assertTrue(inventory.addItem(item));
    }

    @Test
    void testAddItem_FullInventory() {
        InventoryModel inventory = new InventoryModel(1);
        for (int i = 0; i < inventory.getSize(); i++) {
            Item item = new BagPart1();
            assertTrue(inventory.addItem(item));
        }
        assertFalse(inventory.addItem(new BagPart1()));
    }

    @Test
    void testAddItem_NullItem() {
        InventoryModel inventory = new InventoryModel(3);
        assertFalse(inventory.addItem(null));
    }


}