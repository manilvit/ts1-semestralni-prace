package cz.cvut.fel.manilvit.model.game;

import cz.cvut.fel.manilvit.model.items.BagPart1;
import cz.cvut.fel.manilvit.model.items.Item;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameMapModelTest {

    @Test
    void testRemoveItem_ItemExists() {
        GameMapModel gameMapModel = new GameMapModel();
        Item item = gameMapModel.instantiateItem("bagPart1", 1, 1);
        assertTrue(gameMapModel.removeItem(item));
    }

    @Test
    void testRemoveItem_ItemDoesNotExist() {
        GameMapModel gameMapModel = new GameMapModel();
        Item item = new BagPart1();
        gameMapModel.instantiateItem("bagPart1", 1, 1);
        assertFalse(gameMapModel.removeItem(item));
    }

}