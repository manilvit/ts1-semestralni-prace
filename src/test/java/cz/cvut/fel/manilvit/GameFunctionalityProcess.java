package cz.cvut.fel.manilvit;


import cz.cvut.fel.manilvit.controller.game.CollisionController;
import cz.cvut.fel.manilvit.controller.game.GameMapController;
import cz.cvut.fel.manilvit.controller.game.PlayerController;
import cz.cvut.fel.manilvit.controller.input.KeyInputType;
import cz.cvut.fel.manilvit.model.game.GameMapModel;
import cz.cvut.fel.manilvit.model.game.InventoryModel;
import cz.cvut.fel.manilvit.model.game.PlayerModel;
import cz.cvut.fel.manilvit.model.items.Bag;
import cz.cvut.fel.manilvit.model.items.Reciepe;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class GameFunctionalityProcess {


    static Configuration configuration;
    static GameMapController gameMapController;
    static GameMapModel gameMapModel;
    static GameMapModel spyGameMapModel;
    static PlayerController playerController;
    static PlayerModel playerModel;
    static InventoryModel inventoryModel;
    static CollisionController collisionController;


    @BeforeAll
    static void setup() {
        configuration = mock(Configuration.class);
        when(configuration.getTileSize()).thenReturn(32);  // Пример значения tileSize


        gameMapModel = new GameMapModel(true);
        spyGameMapModel = spy(gameMapModel);

        int[][] map = {
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 0, 1},
                {1, 0, 0, 0, 1}
        };

        gameMapModel.instantiateItem("bagPart1", 0, 2);
        gameMapModel.instantiateItem("bagPart2", 0, 2);
        gameMapModel.instantiateItem("keyPart1", 0, 2);
        gameMapModel.instantiateItem("keyPart2", 1, 2);
        gameMapModel.instantiateItem("keyPart3", 2, 2);


        when(spyGameMapModel.getMap()).thenReturn(map);


        gameMapController = new GameMapController(true);
        gameMapController.setModel(spyGameMapModel);


        inventoryModel = new InventoryModel(2);


        playerModel = new PlayerModel();
        playerModel.setDefaultValues(configuration.tileSize * 3, 0, 4, configuration);
        playerModel.setInventoryModel(inventoryModel);

        playerController = new PlayerController(true);
        playerController.setModel(playerModel);


        collisionController = new CollisionController(configuration);

    }


    @Test
    @Order(1)
    public void walkAndPickUpItems() {
        for (int i = 0; i < 20; i++) {
            playerModel.move(KeyInputType.DOWN, collisionController.checkEntityTileIntersection(playerModel, spyGameMapModel));
        }

        System.out.println(playerModel.getWorldX() / 32 + " " + playerModel.getWorldY() / 32);
        System.out.println(inventoryModel.getItems()[0].getName());
    }


    @Test
    @Order(2)
    public void craftBag() {
        inventoryModel.addItemToCraft();
        inventoryModel.setCurrentIdx(1);
        inventoryModel.addItemToCraft();
        inventoryModel.craftItem();

        System.out.println(inventoryModel.getItems()[1].getName());

    }

    @Test
    @Order(3)
    public void equipBag() {
        inventoryModel.addItemToCraft();
        System.out.println(inventoryModel.getSize());
    }


    @Test
    @Order(4)
    public void craftKey() {
        for (int i = 0; i < 20; i++) {
            playerModel.move(KeyInputType.RIGHT, collisionController.checkEntityTileIntersection(playerModel, spyGameMapModel));
        }




        inventoryModel.addItemToCraft();
        inventoryModel.setCurrentIdx(0);
        inventoryModel.addItemToCraft();
        inventoryModel.setCurrentIdx(1);
        inventoryModel.addItemToCraft();
        inventoryModel.craftItem();

       System.out.println(inventoryModel.getItems()[1].getName());
    }


//    @Test
//    @Order(5)
//    public void mineOre() {
//        playerController.mineOre();
//        System.out.println(inventoryModel.getItems()[2].getName());
//    }
}
