package cz.cvut.fel.manilvit.controller.game;

import cz.cvut.fel.manilvit.Configuration;
import cz.cvut.fel.manilvit.controller.input.KeyInputType;
import cz.cvut.fel.manilvit.model.game.GameMapModel;
import cz.cvut.fel.manilvit.model.game.PlayerModel;
import cz.cvut.fel.manilvit.model.game.TileType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class CollisionControllerTest {
    private Configuration configuration;
    private CollisionController collisionController;
    private CollisionController collisionControllerMock;
    private GameMapModel gameMapModel;
    private PlayerModel playerModel;
    private TileType passableTile;
    private TileType notPassableTile;

    @BeforeEach
    public void setUp() {
        // Мокируем зависимости

        configuration = mock(Configuration.class);
        when(configuration.getTileSize()).thenReturn(32);  // Пример значения tileSize

        playerModel = mock(PlayerModel.class);
        gameMapModel = mock(GameMapModel.class);

        collisionController = new CollisionController(configuration);
        collisionControllerMock = spy(collisionController);

        passableTile = mock(TileType.class);
        when(passableTile.isPassable()).thenReturn(true);
        when(passableTile.ordinal()).thenReturn(0);


        notPassableTile = mock(TileType.class);
        when(notPassableTile.isPassable()).thenReturn(false);
        when(notPassableTile.ordinal()).thenReturn(1);

    }


    private int [][] filledArray(int [][] array, int val){
        for (int i = 0; i < array.length; i++) {
            Arrays.fill(array[i], val);
        }
        return array;
    }



    @Test
    public void testCheckEntityTileIntersection_NoCollision() {
        when(playerModel.getWorldX()).thenReturn(0);
        when(playerModel.getWorldY()).thenReturn(20);
        Rectangle solidArea = new Rectangle(8, 16, 32, 32);
        when(playerModel.getSolidArea()).thenReturn(solidArea);
        when(playerModel.getSpeed()).thenReturn(4);
        when(playerModel.getDirection()).thenReturn(KeyInputType.UP);

        int[][] map = new int[10][10];
        map = filledArray(map, 0);
        map[0][0] = 1;
        map[0][1] = 1;
        when(gameMapModel.getMap()).thenReturn(map);


        doReturn(true).when(collisionControllerMock).checkIfNotPassable(1,1);
        doReturn(true).when(collisionControllerMock).checkIfNotPassable(1,0);
        doReturn(true).when(collisionControllerMock).checkIfNotPassable(0,1);

        doReturn(false).when(collisionControllerMock).checkIfNotPassable(0,0);

        boolean result = collisionControllerMock.checkEntityTileIntersection(playerModel, gameMapModel);

        assertFalse(result);
    }


    @Test
    public void testCheckEntityTileIntersection_WithCollision() {
        when(playerModel.getWorldX()).thenReturn(0);
        when(playerModel.getWorldY()).thenReturn(19);
        Rectangle solidArea = new Rectangle(8, 16, 32, 32);
        when(playerModel.getSolidArea()).thenReturn(solidArea);
        when(playerModel.getSpeed()).thenReturn(4);
        when(playerModel.getDirection()).thenReturn(KeyInputType.UP);

        int[][] map = new int[10][10];
        map = filledArray(map, 0);
        map[0][0] = 1;
        map[0][1] = 1;
        when(gameMapModel.getMap()).thenReturn(map);


        doReturn(true).when(collisionControllerMock).checkIfNotPassable(1,1);
        doReturn(true).when(collisionControllerMock).checkIfNotPassable(1,0);
        doReturn(true).when(collisionControllerMock).checkIfNotPassable(0,1);

        doReturn(false).when(collisionControllerMock).checkIfNotPassable(0,0);

        boolean result = collisionControllerMock.checkEntityTileIntersection(playerModel, gameMapModel);

        assertTrue(result);
    }




}