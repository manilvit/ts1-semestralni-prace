package cz.cvut.fel.manilvit.controller.input;

public enum KeyInputType {
    UP, DOWN, LEFT, RIGHT, ESCAPE, ENTER, C, G, R, NONE
}
