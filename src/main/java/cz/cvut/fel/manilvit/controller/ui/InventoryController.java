package cz.cvut.fel.manilvit.controller.ui;

import cz.cvut.fel.manilvit.controller.input.KeyInputType;
import cz.cvut.fel.manilvit.model.game.InventoryModel;

import javax.swing.*;

public class InventoryController {
    private InventoryModel model;
    private Timer timer;
    private boolean canMove;
    private final int DELAY = 150;

    public InventoryController(){
        canMove = true;

        timer = new Timer(DELAY, e -> {
            canMove = true;
        });
        timer.setRepeats(false);
    }


    /**
     * Updates the player controller based on user input.
     * This method moves the player model according to the specified key input type,
     * if the player is allowed to move.
     *
     * @param keyInputType The type of key input event.
     */
    public void update(KeyInputType keyInputType) {
        // Check if the player is allowed to move
        if (canMove) {
            // Move the player model and update movement status
            model.move(keyInputType);
            canMove = false;
            // Restart the movement timer
            timer.restart();
        }
    }

    public void setModel(InventoryModel model) {
        this.model = model;
    }
}
