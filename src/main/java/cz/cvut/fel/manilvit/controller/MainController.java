package cz.cvut.fel.manilvit.controller;

import cz.cvut.fel.manilvit.Configuration;
import cz.cvut.fel.manilvit.controller.game.*;
import cz.cvut.fel.manilvit.controller.input.KeyHandler;
import cz.cvut.fel.manilvit.controller.input.KeyInputType;
import cz.cvut.fel.manilvit.controller.ui.InventoryController;
import cz.cvut.fel.manilvit.controller.ui.MenuController;
import cz.cvut.fel.manilvit.model.game.TileType;
import cz.cvut.fel.manilvit.view.MainView;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainController {

    private final static Logger LOG = Logger.getLogger(MainController.class.getName());


    // initialize in configuration

    public Configuration configuration = new Configuration();

    private GameState gameState = GameState.MAIN_MENU;

    private KeyHandler keyHandler = new KeyHandler();
    private MenuController menuController = new MenuController();

    private InventoryController inventoryController = new InventoryController();

    private PlayerController playerController = new PlayerController();

    private GameMapController gameMapController = new GameMapController(configuration);

    private MainView mainView = new MainView(keyHandler, configuration, playerController.getView(), gameMapController.getView());

    private CollisionController collisionController = new CollisionController(configuration);

    // FPS
    public final int FPS = 60;
    public final int UPS = 60;
    private Thread updateThread;
    private Thread renderThread;
    private boolean running = true;

    private final Object lock = new Object(); // Объект для синхронизации

    private Timer timer;
    private boolean canMove;
    private final int DELAY = 150;


    public MainController() {
        String loggingEnabled = System.getProperty("logging.enabled");
        if (loggingEnabled != null && loggingEnabled.equalsIgnoreCase("true")) {
            LOG.setLevel(Level.INFO); // Включаем логирование, устанавливая уровень INFO
            LOG.addHandler(new ConsoleHandler()); // Добавляем обработчик вывода на консоль
        }


        LOG.info("Main Controller Created!");
        canMove = true;

        timer = new Timer(DELAY, e -> {
            canMove = true;
        });
        timer.setRepeats(false);
    }

    /**
     * Sets the default values for the player controller based on the specified parameters.
     *
     * @param tileNumX The number of tiles in the horizontal direction for the player.
     * @param tileNumY The number of tiles in the vertical direction for the player.
     * @param speed    The speed of the player.
     */
    public void setPlayerDefault(int tileNumX, int tileNumY, int speed) {
        playerController.setDefault(tileNumX, tileNumY, speed, configuration);
    }


    public void startGame() {
        LOG.info("Game Threads Started!");
        running = true;

        UpdateRunnable updateRunnable = new UpdateRunnable(this, lock);
        RenderRunnable renderRunnable = new RenderRunnable(this, lock);

        updateThread = new Thread(updateRunnable);
        renderThread = new Thread(renderRunnable);

        updateThread.start();
        renderThread.start();
    }

    public boolean isRunning() {
        return running;
    }


    /**
     * Updates the game state and controls based on user input.
     * This method is responsible for processing user input, updating the game state, and controlling game flow.
     * It synchronizes access to shared resources using the specified lock object.
     *
     */
    public void update() {
        synchronized (lock) {
            // Get the type of key input received
            KeyInputType keyInputType = keyHandler.getLastKeyPressedEvent();

            // Update game state based on the current state
            switch (gameState) {
                case MAIN_MENU:
                    // Update main menu state
                    gameState = menuController.update(keyInputType, gameState);
                    return;
                case SAVE_MENU:
                    // Update save menu state
                    gameState = menuController.update(keyInputType, gameState);
                    break;
            }

            // Perform actions based on the updated game state
            switch (gameState) {
                case SAVE:
                    // Save player inventory to file and exit the game
                    saveDataToFile(playerController.getModel().generateDataToSaveInventory(), gameMapController.getModel().generateDataToSaveItems());
                    System.exit(0);
                    break;
                case LOAD:
                    // Load player inventory from file and transition to RUN state
                    if (playerController.getModel().loadInventoryFromFile("userSave.txt")
                    && gameMapController.getModel().loadItemsFromFile("userSave.txt")) {
                        inventoryController.setModel(playerController.getModel().getInventoryModel());
                        gameState = GameState.RUN;
                    } else {
                        System.out.println("Cannot load save");
                    }
                    break;
                case NEW_GAME:
                    // Load default player inventory from file and transition to RUN state
                    if (playerController.getModel().loadInventoryFromFile("defaultSave.txt")
                    && gameMapController.getModel().loadItemsFromFile("defaultSave.txt")) {
                        inventoryController.setModel(playerController.getModel().getInventoryModel());
                        gameState = GameState.RUN;
                    } else {
                        System.out.println("Cannot load save");
                    }
                    break;

                case END:
                    System.exit(0);
                    break;

            }

            // Process user input for actions
            if (canMove) {
                switch (keyInputType) {
                    case ESCAPE:
                        // Toggle between RUN and SAVE_MENU states
                        if (gameState == GameState.SAVE_MENU) {
                            gameState = GameState.RUN;
                        } else {
                            gameState = GameState.SAVE_MENU;
                        }
                        break;
                    case C:
                        // Toggle between RUN and CRAFT states
                        if (gameState == GameState.CRAFT) {
                            gameState = GameState.RUN;
                        } else {
                            gameState = GameState.CRAFT;
                        }
                        break;
                }
                // Prevent multiple movements in quick succession
                canMove = false;
                timer.restart();
            }

            // Update game elements based on the current game state
            switch (gameState) {
                case RUN:
                    if (playerController.getModel().isEndTheGame()){
                        gameState = GameState.END;
                        break;
                    }
                    // Process player interactions with tiles and update player controller
                    processInteraction(playerController.getModel().getInteractedWithTile1());
                    playerController.getModel().setInteractedWithTile1(null);
                    processInteraction(playerController.getModel().getInteractedWithTile2());
                    playerController.getModel().setInteractedWithTile2(null);
                    playerController.update(keyInputType, collisionController.checkEntityTileIntersection(playerController.getModel(), gameMapController.getModel()));
                    break;
                case CRAFT:
                    // Update inventory controller for crafting state
                    inventoryController.update(keyInputType);
                    break;
            }
        }
    }

    /**
     * Calls draw method
     */
    public void render() {
        synchronized (lock) {
            mainView.updateGFX(gameState, menuController.getMainMenuModel(), menuController.getSaveMenuModel(), playerController.getModel(), gameMapController.getModel());
        }
    }


    /**
     * Processes the interaction with a tile of the specified type.
     *
     * @param tileType The type of the tile with which interaction occurs.
     */
    private void processInteraction(TileType tileType) {
        if (tileType == null) {
            return;
        }

        System.out.println(tileType);


        switch (tileType) {
            case DOOR:
                LOG.info("Interaction with door proceed");
                if (playerController.getModel().deleteItem("KeyDoor")) {
                    LOG.info("Interaction with door proceed");
                    TileType.DOOR.setIsPassable(true);
                }
                break;
            case WALL:
                LOG.info("Interaction with ore proceed");
                playerController.mineOre();
                break;

        }

    }



    /**
     * Saves inventory data to a file.
     * This method generates save data and writes it to the user's save file.
     */
    public void saveDataToFile(String inventoryData, String itemData) {

        String saveData = inventoryData + itemData;

        // Use the application's working directory to save the file
        File file = new File("saves/userSave.txt");

        // Create directories if they do not exist
        file.getParentFile().mkdirs();

        // If the file already exists, it will be replaced
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, false))) {
            // Write the save data to the file
            writer.write(saveData);
            System.out.println("Data successfully written to file.");

            // Log information about successful data writing
            LOG.info("Data successfully written to file: " + file.getAbsolutePath());

        } catch (IOException e) {
            // Handle IO exception
            System.err.println("An error occurred while writing to file: " + e.getMessage());

            // Log error message
            LOG.info("Error writing data to file: " + file.getAbsolutePath());
        }
    }




    public MainView getMainView() {
        return mainView;
    }


    public PlayerController getPlayerController() {
        return playerController;
    }
}
