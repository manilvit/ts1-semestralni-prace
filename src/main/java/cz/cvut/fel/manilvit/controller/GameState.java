package cz.cvut.fel.manilvit.controller;

public enum GameState {
    RUN, PAUSE, CRAFT, MAIN_MENU, SAVE_MENU, SAVE, LOAD, NEW_GAME, END
}
