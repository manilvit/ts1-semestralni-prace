package cz.cvut.fel.manilvit.controller.game;

import cz.cvut.fel.manilvit.Configuration;
import cz.cvut.fel.manilvit.model.game.GameMapModel;
import cz.cvut.fel.manilvit.model.game.PlayerModel;
import cz.cvut.fel.manilvit.model.game.TileType;
import cz.cvut.fel.manilvit.model.items.Item;

public class CollisionController {

    private Configuration cfg;
    private boolean wasInteractionInLastStep;
    private TileType tileTypeToInteract1;
    private TileType tileTypeToInteract2;

    public CollisionController(Configuration configuration) {
        this.cfg = configuration;
    }

    /**
     * Checks for intersection between the player entity and tiles on the game map.
     * This method determines whether the player entity collides with any solid tiles on the game map.
     *
     * @param entity      The player entity to check for intersection.
     * @param gameMapModel The game map model containing tile information.
     * @return True if there is intersection with solid tiles, false otherwise.
     */
    public boolean checkEntityTileIntersection(PlayerModel entity, GameMapModel gameMapModel) {
        // Get the map and speed of the player entity
        int[][] map = gameMapModel.getMap();
        int speed = entity.getSpeed();

        // Calculate solid area boundaries of the player entity
        int solidAreaLeftWorldX = entity.getWorldX() + entity.getSolidArea().x;
        int solidAreaRightWorldX = solidAreaLeftWorldX + entity.getSolidArea().width;
        int solidAreaTopWorldY = entity.getWorldY() + entity.getSolidArea().y;
        int solidAreaBottomY = solidAreaTopWorldY + entity.getSolidArea().height;

        // Convert solid area boundaries to tile coordinates
        int solidAreaLeftTileNumX = solidAreaLeftWorldX / cfg.getTileSize();
        int solidAreaRightTileNumX = solidAreaRightWorldX / cfg.getTileSize();
        int solidAreaTopTileNumY = solidAreaTopWorldY / cfg.getTileSize();
        int solidAreaBottomTileNumY = solidAreaBottomY / cfg.getTileSize();

        // Initialize variables to store tile IDs
        int tileId1;
        int tileId2;

        // Check intersection based on the direction of movement
        switch (entity.getDirection()) {
            case ENTER:
                // Process interaction if interaction occurred in the last step
                if (wasInteractionInLastStep){
                    processInteraction(entity);
                }
                break;
            case UP:
                // Update top tile coordinate for upward movement
                solidAreaTopTileNumY = (solidAreaTopWorldY - speed) / cfg.getTileSize();
                // Retrieve tile IDs for potential collision
                tileId1 = map[solidAreaTopTileNumY][solidAreaLeftTileNumX];
                tileId2 = map[solidAreaTopTileNumY][solidAreaRightTileNumX];
                // Detect and handle possible tile interactions
                detectPossibleTileInteraction(tileId1, tileId2);
                // Check for impassable tiles
                if(checkIfNotPassable(tileId1, tileId2)){
                    return true;
                }
                // Process item interactions on the top tiles
                processItemInteraction(gameMapModel.getItemByTileNum(solidAreaTopTileNumY, solidAreaLeftTileNumX), entity, gameMapModel);
                processItemInteraction(gameMapModel.getItemByTileNum(solidAreaTopTileNumY, solidAreaRightTileNumX), entity, gameMapModel);
                break;
            case DOWN:
                // Update bottom tile coordinate for downward movement
                solidAreaBottomTileNumY = (solidAreaBottomY + speed) / cfg.getTileSize();
                // Retrieve tile IDs for potential collision
                tileId1 = map[solidAreaBottomTileNumY][solidAreaLeftTileNumX];
                tileId2 = map[solidAreaBottomTileNumY][solidAreaRightTileNumX];
                // Detect and handle possible tile interactions
                detectPossibleTileInteraction(tileId1, tileId2);
                // Check for impassable tiles
                if(checkIfNotPassable(tileId1, tileId2)){
                    return true;
                }
                // Process item interactions on the bottom tiles
                processItemInteraction(gameMapModel.getItemByTileNum(solidAreaBottomTileNumY, solidAreaLeftTileNumX), entity, gameMapModel);
                processItemInteraction(gameMapModel.getItemByTileNum(solidAreaBottomTileNumY, solidAreaRightTileNumX), entity, gameMapModel);
                break;
            case LEFT:
                // Update left tile coordinate for leftward movement
                solidAreaLeftTileNumX = (solidAreaLeftWorldX - speed) / cfg.getTileSize();
                // Retrieve tile IDs for potential collision
                tileId1 = map[solidAreaTopTileNumY][solidAreaLeftTileNumX];
                tileId2 = map[solidAreaBottomTileNumY][solidAreaLeftTileNumX];
                // Detect and handle possible tile interactions
                detectPossibleTileInteraction(tileId1, tileId2);
                // Check for impassable tiles
                if(checkIfNotPassable(tileId1, tileId2)){
                    return true;
                }
                // Process item interactions on the left tiles
                processItemInteraction(gameMapModel.getItemByTileNum(solidAreaTopTileNumY, solidAreaLeftTileNumX), entity, gameMapModel);
                processItemInteraction(gameMapModel.getItemByTileNum(solidAreaBottomTileNumY, solidAreaLeftTileNumX), entity, gameMapModel);
                break;
            case RIGHT:
                // Update right tile coordinate for rightward movement
                solidAreaRightTileNumX = (solidAreaRightWorldX + speed) / cfg.getTileSize();
                // Retrieve tile IDs for potential collision
                tileId1 = map[solidAreaTopTileNumY][solidAreaRightTileNumX];
                tileId2 = map[solidAreaBottomTileNumY][solidAreaRightTileNumX];
                // Detect and handle possible tile interactions
                detectPossibleTileInteraction(tileId1, tileId2);
                // Check for impassable tiles
                if(checkIfNotPassable(tileId1, tileId2)){
                    return true;
                }
                // Process item interactions on the right tiles
                processItemInteraction(gameMapModel.getItemByTileNum(solidAreaTopTileNumY, solidAreaRightTileNumX), entity, gameMapModel);
                processItemInteraction(gameMapModel.getItemByTileNum(solidAreaBottomTileNumY, solidAreaRightTileNumX), entity, gameMapModel);
                break;
        }
        return false;
    }


    private void processInteraction(PlayerModel entity){
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        entity.setInteractedWithTile1(tileTypeToInteract1);
                        entity.setInteractedWithTile2(tileTypeToInteract2);
                        wasInteractionInLastStep = false;
                        tileTypeToInteract1 = null;
                        tileTypeToInteract2 = null;
                    }
                },
                150
        );
    }


    /**
     * Processes the interaction with an item on the game map.
     *
     * @param item         The item with which interaction occurs.
     * @param entity       The player object performing the interaction.
     * @param gameMapModel The model of the game map on which the interaction occurs.
     */
    private void processItemInteraction(Item item, PlayerModel entity, GameMapModel gameMapModel){

        if (item != null && entity.pickUpItem(item)) {
            gameMapModel.removeItem(item);
        }
    }

    boolean checkIfNotPassable(int tileId1, int tileId2){
        if (!TileType.values()[tileId1].isPassable() || !TileType.values()[tileId2].isPassable()) {
            return true;
        }
        return false;

    }


    /**
     * Detects possible interaction with a tile.
     *
     * @param tileId1 The identifier of the first tile.
     * @param tileId2 The identifier of the second tile.
     */
    private void detectPossibleTileInteraction(int tileId1, int tileId2){
        if (TileType.values()[tileId1].isInteractive()) {
            wasInteractionInLastStep = true;
            tileTypeToInteract1 = TileType.values()[tileId1];
        }

        if (TileType.values()[tileId2].isInteractive()) {
            wasInteractionInLastStep = true;
            tileTypeToInteract2 = TileType.values()[tileId2];
        }
    }

}