package cz.cvut.fel.manilvit.controller.game;

import cz.cvut.fel.manilvit.Configuration;
import cz.cvut.fel.manilvit.model.game.GameMapModel;
import cz.cvut.fel.manilvit.view.game.GameMapView;

public class GameMapController {

    private GameMapModel model;
    private GameMapView view;

    public GameMapController(Configuration configuration) {
        model = new GameMapModel();
        view = new GameMapView(model.getTiles(), configuration.itemNames);
    }


    public GameMapController(boolean forTestPurpuses){

    }



    public GameMapModel getModel() {
        return model;
    }

    public GameMapView getView() {
        return view;
    }


    public void setModel(GameMapModel model) {
        this.model = model;
    }
}
