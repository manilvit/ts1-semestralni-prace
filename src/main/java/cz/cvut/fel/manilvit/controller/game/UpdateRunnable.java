package cz.cvut.fel.manilvit.controller.game;

import cz.cvut.fel.manilvit.controller.MainController;

public class UpdateRunnable implements Runnable {
    private final MainController mainController;
    private final Object lock;

    public UpdateRunnable(MainController mainController, Object lock) {
        this.mainController = mainController;
        this.lock = lock;
    }

    @Override
    public void run() {
        long lastTime = System.nanoTime();
        final double nsPerUpdate = 1000000000.0 / mainController.UPS;
        double delta = 0;

        while (mainController.isRunning()) {
            long now = System.nanoTime();
            delta += (now - lastTime) / nsPerUpdate;
            lastTime = now;

            while (delta >= 1) {
                mainController.update();
                delta--;
            }

            try {
                Thread.sleep(1); // Маленькая задержка для снижения нагрузки на процессор
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}