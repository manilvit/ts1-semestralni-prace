package cz.cvut.fel.manilvit.controller.game;

import cz.cvut.fel.manilvit.controller.MainController;

public class RenderRunnable implements Runnable {
    private final MainController mainController;
    private final Object lock;

    public RenderRunnable(MainController mainController, Object lock) {
        this.mainController = mainController;
        this.lock = lock;
    }

    /**
     * The main execution loop of the game thread. This method continuously renders frames
     * based on the specified frames per second (FPS) rate until the game is no longer running.
     * It regulates the frame rate by calculating delta time and applying a small delay to reduce CPU load.
     */
    @Override
    public void run() {
        long lastTime = System.nanoTime();
        final double nsPerFrame = 1000000000.0 / mainController.FPS;
        double delta = 0;

        while (mainController.isRunning()) {
            long now = System.nanoTime();
            delta += (now - lastTime) / nsPerFrame;
            lastTime = now;

            if (delta >= 1) {
                mainController.render();
                delta--;
            }

            try {
                Thread.sleep(1); // Маленькая задержка для снижения нагрузки на процессор
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}