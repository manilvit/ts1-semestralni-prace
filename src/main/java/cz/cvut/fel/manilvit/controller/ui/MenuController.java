package cz.cvut.fel.manilvit.controller.ui;

import cz.cvut.fel.manilvit.controller.GameState;
import cz.cvut.fel.manilvit.controller.input.KeyInputType;
import cz.cvut.fel.manilvit.model.ui.MainMenuModel;
import cz.cvut.fel.manilvit.model.ui.SaveMenuModel;

import javax.swing.*;


public class MenuController {

    private MainMenuModel mainMenuModel;
    private SaveMenuModel saveMenuModel;
    private Timer timer;
    private boolean canMove;
    private final int DELAY = 150;

    public MenuController() {

        mainMenuModel = new MainMenuModel();
        saveMenuModel = new SaveMenuModel();
        canMove = true;

        // Создание таймера
        timer = new Timer(DELAY, e -> {
            // Позволяем перемещение после истечения интервала
            canMove = true;
        });
        timer.setRepeats(false); // Таймер не повторяется автоматически
    }

    /**
     * Updates the game state based on user input and current game state.
     * This method processes user input to navigate through menus and updates the game state accordingly.
     * It also controls the movement status and timer restart based on the game state.
     *
     * @param keyInputType The type of key input event.
     * @param gameState    The current game state.
     * @return The updated game state after processing user input.
     */
    public GameState update(KeyInputType keyInputType, GameState gameState) {
        // Check the current game state and process user input accordingly
        if (gameState == GameState.MAIN_MENU && canMove) {
            // Update the game state based on main menu interaction
            gameState = mainMenuModel.move(keyInputType);
            // Prevent movement until the next interval and restart the timer
            canMove = false;
            timer.restart();
        } else if (gameState == GameState.SAVE_MENU && canMove) {
            // Update the game state based on save menu interaction
            gameState = saveMenuModel.move(keyInputType);
            // Prevent movement until the next interval and restart the timer
            canMove = false;
            timer.restart();
        }
        return gameState;
    }


    public MainMenuModel getMainMenuModel() {
        return mainMenuModel;
    }

    public SaveMenuModel getSaveMenuModel() {
        return saveMenuModel;
    }
}
