package cz.cvut.fel.manilvit.controller.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static java.awt.event.KeyEvent.*;

public class KeyHandler implements KeyListener {
    private KeyInputType lastKeyPressedEvent = KeyInputType.NONE;

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();

        switch (code){
            case VK_W:
                lastKeyPressedEvent = KeyInputType.UP;
                break;
            case VK_S:
                lastKeyPressedEvent = KeyInputType.DOWN;
                break;
            case VK_A:
                lastKeyPressedEvent = KeyInputType.LEFT;
                break;
            case VK_D:
                lastKeyPressedEvent = KeyInputType.RIGHT;
                break;
            case VK_ESCAPE:
                lastKeyPressedEvent = KeyInputType.ESCAPE;
                break;
            case VK_C:
                lastKeyPressedEvent = KeyInputType.C;
                break;
            case VK_ENTER:
                lastKeyPressedEvent = KeyInputType.ENTER;
                break;
            case VK_G:
                lastKeyPressedEvent = KeyInputType.G;
                break;
            case VK_R:
                lastKeyPressedEvent = KeyInputType.R;
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int code = e.getKeyCode();

        switch (code) {
            case KeyEvent.VK_W:
                if (lastKeyPressedEvent == KeyInputType.UP) {
                    lastKeyPressedEvent = KeyInputType.NONE;
                }
                break;
            case KeyEvent.VK_S:
                if (lastKeyPressedEvent == KeyInputType.DOWN) {
                    lastKeyPressedEvent = KeyInputType.NONE;
                }
                break;
            case KeyEvent.VK_A:
                if (lastKeyPressedEvent == KeyInputType.LEFT) {
                    lastKeyPressedEvent = KeyInputType.NONE;
                }
                break;
            case KeyEvent.VK_D:
                if (lastKeyPressedEvent == KeyInputType.RIGHT) {
                    lastKeyPressedEvent = KeyInputType.NONE;
                }
                break;
            case KeyEvent.VK_ESCAPE:
                if (lastKeyPressedEvent == KeyInputType.ESCAPE) {
                    lastKeyPressedEvent = KeyInputType.NONE;
                }
                break;
            case KeyEvent.VK_C:
                if (lastKeyPressedEvent == KeyInputType.C) {
                    lastKeyPressedEvent = KeyInputType.NONE;
                }
                break;
            case KeyEvent.VK_ENTER:
                if (lastKeyPressedEvent == KeyInputType.ENTER) {
                    lastKeyPressedEvent = KeyInputType.NONE;
                }
                break;
            case KeyEvent.VK_G:
                if (lastKeyPressedEvent == KeyInputType.G) {
                    lastKeyPressedEvent = KeyInputType.NONE;
                }
                break;
            case KeyEvent.VK_R:
                if (lastKeyPressedEvent == KeyInputType.R) {
                    lastKeyPressedEvent = KeyInputType.NONE;
                }
                break;
        }
    }

    public KeyInputType getLastKeyPressedEvent() {
        return lastKeyPressedEvent;
    }



}
