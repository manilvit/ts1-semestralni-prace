package cz.cvut.fel.manilvit.controller.game;

import cz.cvut.fel.manilvit.Configuration;
import cz.cvut.fel.manilvit.controller.input.KeyInputType;
import cz.cvut.fel.manilvit.model.game.PlayerModel;
import cz.cvut.fel.manilvit.model.items.BagPart2;
import cz.cvut.fel.manilvit.view.game.PlayerView;

public class PlayerController {

    private PlayerModel model;
    private PlayerView view;

    public PlayerController(){
        model = new PlayerModel();
        view = new PlayerView();
    }

    public PlayerController(boolean forTestPurpuses){

    }

    public void setModel(PlayerModel model) {
        this.model = model;
    }

    /**
     * Sets the default values for the game model based on the specified parameters.
     *
     * @param tileNumX       The number of tiles in the horizontal direction.
     * @param tileNumY       The number of tiles in the vertical direction.
     * @param speed          The speed of the game.
     * @param configuration  The configuration settings for the game.
     */
    public void setDefault(int tileNumX, int tileNumY, int speed, Configuration configuration){
        model.setDefaultValues(tileNumX, tileNumY, speed, configuration);
    }



    /**
     * Updates the player controller based on user input and collision detection.
     * This method moves the player model according to the specified key input type and collision status,
     * increments the sprite counter in the view, and toggles the sprite if the counter exceeds a threshold.
     *
     * @param keyEventType The type of key input event.
     * @param collision    A boolean value indicating whether a collision has occurred.
     */
    public void update(KeyInputType keyEventType, boolean collision) {

        // Move the player model based on the key input type and collision status
        model.move(keyEventType, collision);

        // Increment the sprite counter in the view
        view.incrementSpriteCounter();

        // Toggle the sprite and reset the counter if it exceeds the threshold
        if (view.getSpriteCounter() > 15) {
            view.toggleSprite();
            view.resetSpriteCounter();
        }
    }


    public PlayerModel getModel() {
        return model;
    }

    public PlayerView getView() {
        return view;
    }
    public void mineOre() {
        model.getInventoryModel().addItem(new BagPart2(-1, -1));
    }
}
