package cz.cvut.fel.manilvit;

import cz.cvut.fel.manilvit.controller.MainController;
import cz.cvut.fel.manilvit.view.MainView;

import javax.swing.JFrame;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Main class is the entry point of the application.
 * It creates a JFrame window and starts the game by adding
 * a GamePanel to it.
 */
public class Main {
    private final static Logger LOG = Logger.getLogger(Main.class.getName());

    /**
     * The main method, which is the entry point of the application.
     * It creates a JFrame window, sets up the game panel, and starts
     * the game thread.
     *
     * @param args The command-line arguments (not used in this application).
     */
    public static void main(String[] args) {


        System.setProperty("logging.enabled", "true");

        String loggingEnabled = System.getProperty("logging.enabled");
        if (loggingEnabled != null && loggingEnabled.equalsIgnoreCase("true")) {
            LOG.setLevel(Level.INFO);
            LOG.addHandler(new ConsoleHandler());
        } else {
            Logger rootLogger = Logger.getLogger("");
            for (Handler handler : rootLogger.getHandlers()) {
                rootLogger.removeHandler(handler);
            }
        }

        LOG.info("Main started");


        JFrame window = new JFrame();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.setTitle("2D Adventure");

        MainController mainController = new MainController();

        MainView mainView = mainController.getMainView();

        // 1 and 1 is a tile number where character will spawn
        mainController.setPlayerDefault(10, 10, 5);


        window.add(mainView);

        window.pack();

        window.setLocationRelativeTo(null);	// It will be in the center of the screen
        window.setVisible(true);

        mainController.startGame();


//        gamePanel.startGameThread();
    }
}