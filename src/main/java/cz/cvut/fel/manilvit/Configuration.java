package cz.cvut.fel.manilvit;


import java.util.ArrayList;
import java.util.List;

// add getter
public class Configuration {


    public final int tileSize; //48x48
    public final int maxTileNumX = 16;
    public final int maxTileNumY = 12;
    public final int screenWidth; // 768px
    public final int screenHeight; // 576px


    public final List<String> itemNames;


    public Configuration(){
        this.tileSize = 48;
        this.screenWidth = tileSize * maxTileNumX;
        this.screenHeight = tileSize * maxTileNumY;




        this.itemNames = new ArrayList<>();

        itemNames.add("BagPart1");
        itemNames.add("BagPart2");
        itemNames.add("Bag");

        itemNames.add("KeyDoor");
        itemNames.add("KeyPart1");
        itemNames.add("KeyPart2");
        itemNames.add("KeyPart3");

        itemNames.add("Key");

        itemNames.add("Boots");

        itemNames.add("GameEnd");




    }


    public int getTileSize() {
        return tileSize;
    }
}
