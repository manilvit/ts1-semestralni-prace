package cz.cvut.fel.manilvit.model.ui;

public abstract class MenuModel {
    String [] menuItemNames;
    int currentIdx;


    public String[] getMenuItemNames() {
        return menuItemNames;
    }

    public int getCurrentIdx() {
        return currentIdx;
    }
}
