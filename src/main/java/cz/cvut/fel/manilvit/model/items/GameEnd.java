package cz.cvut.fel.manilvit.model.items;

public class GameEnd extends Item {
    public GameEnd(){
        name = "GameEnd";
    }

    public GameEnd(int tileNumX, int tileNumY){
        super(tileNumX, tileNumY);
        name = "GameEnd";
    }
}
