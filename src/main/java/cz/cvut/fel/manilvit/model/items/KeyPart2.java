package cz.cvut.fel.manilvit.model.items;

public class KeyPart2 extends Item{
    public KeyPart2(){
        name = "KeyPart2";
    }

    public KeyPart2(int tileNumX, int tileNumY){
        super(tileNumX, tileNumY);
        name = "KeyPart2";
    }
}
