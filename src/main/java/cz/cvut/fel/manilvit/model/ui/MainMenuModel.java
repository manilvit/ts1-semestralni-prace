package cz.cvut.fel.manilvit.model.ui;

import cz.cvut.fel.manilvit.controller.GameState;
import cz.cvut.fel.manilvit.controller.input.KeyInputType;

public class MainMenuModel extends MenuModel{

    public MainMenuModel(){
        this.currentIdx = 0;
        this.menuItemNames = new String[3];
        this.menuItemNames[0] = "New Game";
        this.menuItemNames[1] = "Continue";
        this.menuItemNames[2] = "Exit Game";
    }


    private GameState executeItem(int currentIdx){
        GameState gameState = GameState.MAIN_MENU;
        switch (currentIdx){
            case 0:
                gameState = GameState.NEW_GAME;
                break;
            case 1:
                gameState = GameState.LOAD;
                break;
            case 2:
                System.exit(0);
                break;
        }
        return gameState;
    }


    public GameState move(KeyInputType keyInputType) {
        GameState gameState = GameState.MAIN_MENU;
        switch (keyInputType) {
            case ENTER:
                gameState = executeItem(currentIdx);
            case DOWN:
                currentIdx += 1;
                currentIdx %= 3;
                break;
            case UP:
                currentIdx -= 1;
                if (currentIdx == -1){
                    currentIdx = 2;
                }
                break;
        }
        return gameState;
    }
}
