package cz.cvut.fel.manilvit.model.items;

public class Boots extends Item{

    private int speedBoost = 4;

    public Boots(){
        name = "Boots";
    }

    public Boots(int tileNumX, int tileNumY){
        super(tileNumX, tileNumY);
        name = "Boots";
    }


    public int getSpeedBoost() {
        return speedBoost;
    }

}
