package cz.cvut.fel.manilvit.model.items;

import java.util.ArrayList;
import java.util.List;

public class Reciepe {


    private List<String> ingredientNames;
    private Item result;

    public Reciepe(List<String> ingredientNames, Item result){
        this.ingredientNames = new ArrayList<>(ingredientNames);
        this.result = result;
    }


    public Item getResult() {
        return result;
    }

    public Item tryCraft(List<String> itemNames) {
        if (itemNames != null && itemNames.size() == ingredientNames.size() && itemNames.containsAll(ingredientNames)){
            return result;
        }
        return null;
    }
}
