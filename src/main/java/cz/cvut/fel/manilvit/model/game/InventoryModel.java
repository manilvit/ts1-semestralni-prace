package cz.cvut.fel.manilvit.model.game;

import cz.cvut.fel.manilvit.controller.input.KeyInputType;
import cz.cvut.fel.manilvit.model.items.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InventoryModel {

    private List <Reciepe> reciepes;
    private Item [] items;
    private Item [] craft;
    private int size;
    private int currentIdx;



    public InventoryModel(int size){
        this.size = size;
        this.items = new Item[5];
        this.craft = new Item[3];
        this.reciepes = new ArrayList<>();
        initializeReciepes();
    }

    private void initializeReciepes(){
        List<String> ingerdients = new ArrayList<>();
        ingerdients.add("BagPart1");
        ingerdients.add("BagPart2");
        reciepes.add(new Reciepe(ingerdients, new Bag()));

        ingerdients.clear();

        ingerdients.add("KeyPart1");
        ingerdients.add("KeyPart2");
        ingerdients.add("KeyPart3");

        reciepes.add(new Reciepe(ingerdients, new Key()));

        ingerdients.clear();

        ingerdients.add("BagPart2");
        ingerdients.add("Key");
        reciepes.add(new Reciepe(ingerdients, new KeyDoor()));

    }




    // 0 ok, 1 already in so remove it, 2 there is no place
    public void addItemToCraft(){
        if (items[currentIdx] != null && items[currentIdx].getName().equals("Bag")){
            size = 3;
            items[currentIdx] = null;
            return;
        }

        if (items[currentIdx] == null) {
            return;
        }

        for (int i = 0; i < craft.length; i++){
            if (items[currentIdx] == craft[i]){
                items[currentIdx].setAddedToCraft(false);
                craft[i] = null;
                return;
            }
        }

        for (int i = 0; i < craft.length; i++){
            if (null == craft[i]){
                items[currentIdx].setAddedToCraft(true);
                craft[i] = items[currentIdx];
                return;
            }
        }

    }

    private void removeItem(){
        items[currentIdx] = null;
    }

    public void move(KeyInputType keyInputType) {
        switch (keyInputType) {
            case R:
                removeItem();
                break;
            case G:
                craftItem();
                break;
            case ENTER:
                addItemToCraft();
                break;
            case RIGHT:
                currentIdx += 1;
                currentIdx %= getSize();
                break;
            case LEFT:
                currentIdx -= 1;
                if (currentIdx == -1){
                    currentIdx = getSize() - 1;
                }
                break;
        }
    }

    public void craftItem() {
        List<String> itemNames = extractItemNames();
        if (!itemNames.isEmpty()){
            processRecipes(itemNames);

        }
    }

    private List<String> extractItemNames() {
        List<String> itemNames = new ArrayList<>();

        for (Item item : craft) {
            if (item != null) {
                itemNames.add(item.getName());
            }
        }

        return itemNames;
    }

    private void processRecipes(List<String> itemNames) {
        for (Reciepe reciepe : reciepes) {
            Item craftedItem = tryCraftItem(reciepe, itemNames);

            if (craftedItem != null) {
                int indexForCraftedItem = findIndexForCraftedItem();
                items[indexForCraftedItem] = craftedItem;
                Arrays.fill(craft, null);
            }
        }
    }

    private Item tryCraftItem(Reciepe reciepe, List<String> itemNames) {
        return reciepe.tryCraft(itemNames);
    }

    /**
     * Finds the index in the array for the crafted item that needs to be added.
     * This method iterates through the array of items to find the index of the item
     * that has been marked as added to craft. It returns the index of the first such item found,
     * or -1 if no such item is found.
     *
     * @return The index of the crafted item to be added, or -1 if no crafted item is found.
     */
    public int findIndexForCraftedItem() {
        int indexForCraftedItem = -1;

        for (int i = 0; i < size; i++) {
            if (items[i] != null && items[i].isAddedToCraft()) {
                items[i] = null;
                indexForCraftedItem = i;
            }
        }

        return indexForCraftedItem;
    }


    /**
     * Adds an item to the array of items.
     * This method attempts to add the specified item to the array of items.
     * It iterates through the array to find the first available slot and adds the item there.
     * If the item is successfully added, it returns true; otherwise, it returns false.
     *
     * @param item The item to add to the array.
     * @return True if the item is successfully added, false otherwise.
     */
    public boolean addItem(Item item){
        for (int i = 0; i < size; i++) {
            if (items[i] == null && item != null){
                items[i] = item;
                return true;
            }
        }
        return false;
    }

    /**
     * Adds an item to the array of items at the specified index.
     * This method adds the specified item to the array of items at the given index,
     * if the index is within the bounds of the array.
     *
     * @param item  The item to add to the array.
     * @param index The index at which to add the item.
     */
    public void addItemByIndex(Item item,int index){
        if (index < size){
            items[index] = item;
        }
    }

    public Item[] getItems() {
        return items;
    }

    public void setItems(Item[] items) {
        this.items = items;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
    public int getCurrentIdx() {
        return currentIdx;
    }

    public Item[] getCraft() {
        return craft;
    }
    /**
     * Deletes the item with the specified name from the array of items.
     * This method iterates through the array of items to find an item with the given name.
     * If such an item is found, it is removed from the array by setting its reference to null,
     * and the method returns true. If no item with the given name is found, the method returns false.
     *
     * @param name The name of the item to delete from the array.
     * @return True if the item with the specified name is successfully deleted, false otherwise.
     */
    public boolean deleteItem(String name) {
        for (int i = 0; i < size; i++) {
            if (items[i] != null && items[i].getName().equals(name)){
                items[i] = null;
                return true;
            }
        }
        return false;
    }


    /**
     * Checks if the array of items contains an item with the specified name.
     * This method iterates through the array of items to check if there is an item
     * with the given name. If such an item is found, the method returns true; otherwise, it returns false.
     *
     * @param name The name of the item to check for in the array.
     * @return True if the array contains an item with the specified name, false otherwise.
     */
    public boolean contains(String boots) {
        for (Item item : items){
            if (item != null && item.getName().equals(boots)){
                return  true;
            }
        }
        return false;
    }


    public List<Reciepe> getReciepes() {
        return reciepes;
    }

    public void setReciepes(List<Reciepe> reciepes) {
        this.reciepes = reciepes;
    }

    public void setCurrentIdx(int currentIdx) {
        this.currentIdx = currentIdx;
    }
}
