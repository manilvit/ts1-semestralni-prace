package cz.cvut.fel.manilvit.model.game;

import cz.cvut.fel.manilvit.controller.input.KeyInputType;

import java.awt.*;

public class EntityModel {
    protected int worldX;
    protected int worldY;
    protected int speed = 5;
    protected KeyInputType keyInputType;

    protected Rectangle solidArea;

    public int getWorldX() {
        return worldX;
    }

    public void setWorldX(int worldX) {
        this.worldX = worldX;
    }

    public int getWorldY() {
        return worldY;
    }

    public void setWorldY(int worldY) {
        this.worldY = worldY;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public KeyInputType getDirection() {
        return keyInputType;
    }

    public void setDirection(KeyInputType keyInputType) {
        this.keyInputType = keyInputType;
    }

    public Rectangle getSolidArea() {
        return solidArea;
    }
}
