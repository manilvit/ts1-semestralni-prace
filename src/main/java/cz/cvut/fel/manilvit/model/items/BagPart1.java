package cz.cvut.fel.manilvit.model.items;

public class BagPart1 extends Item{

    public BagPart1(){
        name = "BagPart1";
    }

    public BagPart1(int tileNumX, int tileNumY){
        super(tileNumX, tileNumY);
        name = "BagPart1";
    }


}
