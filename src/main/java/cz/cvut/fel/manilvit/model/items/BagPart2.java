package cz.cvut.fel.manilvit.model.items;

public class BagPart2 extends Item{
    public BagPart2(){
        name = "BagPart2";
    }

    public BagPart2(int tileNumX, int tileNumY){
        super(tileNumX, tileNumY);
        name = "BagPart2";
    }
}
