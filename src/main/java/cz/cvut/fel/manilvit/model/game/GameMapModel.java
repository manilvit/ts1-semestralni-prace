package cz.cvut.fel.manilvit.model.game;

import cz.cvut.fel.manilvit.controller.MainController;
import cz.cvut.fel.manilvit.model.items.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;


public class GameMapModel {
    private final static Logger LOG = Logger.getLogger(GameMapModel.class.getName());


    private int maxTileNumX;
    private int maxTileNumY;
    private int [][] map;

    private List<TileType> tiles;

    private List<Item> items;

    public GameMapModel(){
        String loggingEnabled = System.getProperty("logging.enabled");
        if (loggingEnabled != null && loggingEnabled.equalsIgnoreCase("true")) {
            LOG.setLevel(Level.INFO); // Включаем логирование, устанавливая уровень INFO
            LOG.addHandler(new ConsoleHandler()); // Добавляем обработчик вывода на консоль
        }

        tiles = new ArrayList<>();
        items = new ArrayList<>();
        initializeTiles();
        loadMapFromFile("/maps/world01.txt");
    }

    public GameMapModel(boolean forTestPurpuses){
        tiles = new ArrayList<>();
        items = new ArrayList<>();
        initializeTiles();
    }



    /**
     * Initializes various types of tiles and adds them to the collection of tiles.
     * Each tile type is configured with specific properties such as passability and interactivity.
     */
    private void initializeTiles(){
        TileType grassTile = TileType.GRASS;
        grassTile.setIsPassable(true);
        tiles.add(grassTile);
        LOG.info("Tile "+ grassTile + " was initialized!");

        TileType wallTile = TileType.WALL;
        wallTile.setInteractive(true);
        tiles.add(wallTile);
        LOG.info("Tile "+ wallTile + " was initialized!");

        TileType waterTile = TileType.WATER;
        tiles.add(waterTile);
        LOG.info("Tile "+ waterTile + " was initialized!");

        TileType earthTile = TileType.EARTH;
        earthTile.setIsPassable(true);
        tiles.add(earthTile);
        LOG.info("Tile "+ earthTile + " was initialized!");

        TileType treeTile = TileType.TREE;
        tiles.add(treeTile);
        LOG.info("Tile "+ treeTile + " was initialized!");

        TileType sandTile = TileType.SAND;
        sandTile.setIsPassable(true);
        tiles.add(sandTile);
        LOG.info("Tile "+ sandTile + " was initialized!");

        TileType doorTile = TileType.DOOR;
        doorTile.setInteractive(true);
        tiles.add(doorTile);
        LOG.info("Tile "+ doorTile + " was initialized!");

    }



    /**
     * Loads items data from a specified file.
     * This method reads item data from the given source file, instantiates items based on the data,
     * and adds them to the game environment. It returns true if items are successfully loaded, otherwise false.
     *
     * @param sourceFile The name of the file containing items data.
     * @return True if items are successfully loaded, false otherwise.
     */
    public boolean loadItemsFromFile(String sourceFile){
        boolean itemsDetected = false;


        // Use the application's working directory to read the file
        File file = new File("saves/" + sourceFile);

        // Check if the file exists
        if (!file.exists()) {
            // Print a message if the file is not found and load the default save file instead
            System.out.println("File not found: " + sourceFile + ", loading default file.");
            file = new File("saves/defaultSave.txt");
        }

        // If the default file is also not found, return false
        if (!file.exists()) {
            // Log a warning message if the default file is not found
//            LOG.info("Default save file not found: " + file.getAbsolutePath());
            return false;
        }

        // Read data from the file
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(" ");
                if (parts[0].equals("Items")) {
                    itemsDetected = true;
                } else if (itemsDetected){
                    instantiateItem(parts[0], Integer.parseInt(parts[1]), Integer.parseInt(parts[2]));
                }
            }
            return true;
        } catch (IOException e) {
            // Handle IO exception
//            LOG.info("Error reading data from file: " + file.getAbsolutePath());
        }
        return false;
    }


    /**
     * Instantiates an item object based on the given item name and tile coordinates.
     *
     * @param itemName The name of the item to instantiate.
     * @param tileNumX The x-coordinate of the tile where the item will be placed.
     * @param tileNumY The y-coordinate of the tile where the item will be placed.
     * @return The newly instantiated item object, or null if the itemName is not recognized.
     */
    public Item instantiateItem(String itemName, int tileNumX, int tileNumY) {
        Item newItem = null;
        switch (itemName){
            case "bagPart1":
                newItem = new BagPart1(tileNumX, tileNumY);
                break;
            case "bagPart2":
                newItem = new BagPart2(tileNumX, tileNumY);
                break;
            case "keyPart1":
                newItem = new KeyPart1(tileNumX, tileNumY);
                break;
            case "keyPart2":
                newItem = new KeyPart2(tileNumX, tileNumY);
                break;
            case "keyPart3":
                newItem = new KeyPart3(tileNumX, tileNumY);
                break;
            case "boots":
                newItem = new Boots(tileNumX, tileNumY);
                break;
            case "gameEnd":
                newItem = new GameEnd(tileNumX, tileNumY);
                break;
        }
        if (newItem != null) {
            items.add(newItem);
        }
        return newItem;
    }


    public boolean removeItem(Item item){

        if (item != null && items.remove(item)){
            LOG.info(item.getName() + "was removed from game map!");
            return true;
        }
        return false;
    }

    /**
     * Loads a map from a file.
     * This method reads tile IDs from the specified file path and initializes the game map accordingly.
     *
     * @param filePath The path to the file containing tile IDs.
     */
    private void loadMapFromFile(String filePath) {
        try {
            // Open an input stream to read the file
            InputStream is = getClass().getResourceAsStream(filePath);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            // Initialize a list to store tile ID strings
            List<String[]> idsString = new ArrayList<>();
            String line;

            // Read tile IDs from each line and calculate maxTileNumX and maxTileNumY
            while ((line = br.readLine()) != null) {
                String[] tileIdsString = line.split(" ");
                idsString.add(tileIdsString);
                maxTileNumX = Math.max(maxTileNumX, line.length());
                maxTileNumY++;
            }

            // Initialize the map array
            map = new int[maxTileNumY][maxTileNumX];

            // Populate the map array with tile IDs
            int i = 0;
            int j = 0;
            for (String[] idsLine : idsString) {
                for (String idString : idsLine) {
                    map[i][j] = Integer.parseInt(idString);
                    j++;
                }
                i++;
                j = 0;
            }

            // Log information about successful map loading
            LOG.info("Map loaded successfully from file: " + filePath);

        } catch (IOException e) {
            // Handle IO exception
            LOG.info("Error loading map from file: " + filePath);
        }
    }


    public Item getItemByTileNum(int tileNumY, int tileNumX){
        for (Item item : items){
            if (item.getTileNumX() == tileNumX && item.getTileNumY() == tileNumY){
                return item;
            }
        }
        return null;
    }

    public int[][] getMap() {
        return map;
    }

    public List<TileType> getTiles() {
        return tiles;
    }

    public int getMaxTileNumX() {
        return maxTileNumX;
    }

    public int getMaxTileNumY() {
        return maxTileNumY;
    }


    public void setMap(int[][] map) {
        this.map = map;
    }



    /**
     * Generates a string containing data about items to be saved.
     * This method iterates through the list of items and constructs a string representation
     * containing each item's name, position (tile coordinates), and appends it to the data string.
     *
     * @return A string containing data about items in a format suitable for saving.
     */
    public String generateDataToSaveItems() {
        StringBuilder data = new StringBuilder("\nItems");

        for (Item item : items) {
            if (item != null) {
                data.append("\n" + item.getName().substring(0,1).toLowerCase() + item.getName().substring(1) + " " + item.getTileNumX() + " " + item.getTileNumY());
            }
        }

        return data.toString();
    }
}
