package cz.cvut.fel.manilvit.model.game;


import cz.cvut.fel.manilvit.Configuration;
import cz.cvut.fel.manilvit.controller.MainController;
import cz.cvut.fel.manilvit.controller.input.KeyInputType;
import cz.cvut.fel.manilvit.model.items.*;

import java.awt.*;
import java.io.*;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.cvut.fel.manilvit.controller.input.KeyInputType.*;

public class PlayerModel extends EntityModel {
    private final static Logger LOG = Logger.getLogger(PlayerModel.class.getName());


    private int screenX;
    private int screenY;

    private InventoryModel inventoryModel;
    private TileType interactedWithTile1;
    private TileType interactedWithTile2;

    private boolean endTheGame = false;

    public boolean isEndTheGame() {
        return endTheGame;
    }

    public void setEndTheGame(boolean endTheGame) {
        this.endTheGame = endTheGame;
    }

    public void setDefaultValues(int tileNumX, int tileNumY, int speed, Configuration configuration) {
        String loggingEnabled = System.getProperty("logging.enabled");
        if (loggingEnabled != null && loggingEnabled.equalsIgnoreCase("true")) {
            LOG.setLevel(Level.INFO); // Включаем логирование, устанавливая уровень INFO
            LOG.addHandler(new ConsoleHandler()); // Добавляем обработчик вывода на консоль
        }

        LOG.info("Players default setting was set!");

        int tileSize = configuration.tileSize;

        this.worldX = tileNumX * tileSize;
        this.worldY = tileNumY * tileSize;
        // 12 и 16 это макс кол-во тайлов на экране
        this.screenX = (configuration.maxTileNumX / 2) * tileSize - tileSize / 2;
        this.screenY = (configuration.maxTileNumY / 2) * tileSize - tileSize / 2;
        this.speed = speed;
        this.keyInputType = NONE;

        // COLLISION
        solidArea = new Rectangle(8, 16, 32, 32);

    }

    public String generateDataToSaveInventory() {
        StringBuilder data = new StringBuilder("s " + inventoryModel.getSize());
        int i = 0;

        for (Item item : inventoryModel.getItems()) {
            if (item != null) {
                data.append("\n" + i + " " + item.getName());
            }
            i++;
        }

        return data.toString();
    }




    /**
     * Loads inventory data from a specified file.
     * This method reads inventory data from the specified file path, or from the default save file if the specified file is not found.
     *
     * @param sourceFile The path to the source file containing inventory data.
     * @return True if the inventory is successfully loaded, false otherwise.
     */
    public boolean loadInventoryFromFile(String sourceFile) {
        boolean itemsDetected = false;

        // Use the application's working directory to read the file
        File file = new File("saves/" + sourceFile);

        // Check if the file exists
        if (!file.exists()) {
            // Print a message if the file is not found and load the default save file instead
            System.out.println("File not found: " + sourceFile + ", loading default file.");
            file = new File("saves/defaultSave.txt");
        }

        // If the default file is also not found, return false
        if (!file.exists()) {
            // Log a warning message if the default file is not found
            LOG.info("Default save file not found: " + file.getAbsolutePath());
            return false;
        }

        // Read data from the file
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(" ");
                if (line.charAt(0) == 's') {
                    // Create a new inventory model with the specified capacity
                    inventoryModel = new InventoryModel(Integer.parseInt(parts[1]));
                }else if (parts[0].equals("Items")){
                    itemsDetected = true;
                }else if (!itemsDetected) {
                    // Create and add item instances to the inventory model
                    Item item = createItemInstance(parts[1]);
                    if (item != null) {
                        inventoryModel.addItemByIndex(item, Integer.parseInt(parts[0]));
                    } else {
                        // Handle the case where item creation fails
                        LOG.info("Failed to create item: " + parts[1]);
                    }
                }
            }
            return true;
        } catch (IOException e) {
            // Handle IO exception
            LOG.info("Error reading data from file: " + file.getAbsolutePath());
        }
        return false;
    }

    /**
     * Moves the player character based on the provided key input type and collision status.
     * This method adjusts the player's position (worldX and worldY coordinates) based on the provided
     * key input type and collision status. If there is no collision (collision = false), the player
     * character moves in the specified direction according to the speed. The method also updates the
     * current key input type to reflect the direction of movement.
     *
     * @param keyInputType The type of key input received (UP, DOWN, LEFT, RIGHT, ENTER, or NONE).
     * @param collision    A boolean value indicating whether a collision has occurred with an obstacle.
     */
    public void move(KeyInputType keyInputType, boolean collision) {
        switch (keyInputType) {
            case UP:
                if (!collision) {
                    worldY -= speed;
                }
                this.keyInputType = UP;
                break;
            case DOWN:
                if (!collision) {
                    worldY += speed;
                }
                this.keyInputType = DOWN;
                break;
            case LEFT:
                if (!collision) {
                    worldX -= speed;
                }
                this.keyInputType = LEFT;
                break;
            case RIGHT:
                if (!collision) {
                    worldX += speed;
                }
                this.keyInputType = RIGHT;
                break;
            case ENTER:
                this.keyInputType = ENTER;
                break;
            case NONE:
                this.keyInputType = NONE;
                break;
        }

    }

    /**
     * Picks up an item and adds it to the player's inventory.
     * This method attempts to pick up the specified item and add it to the player's inventory.
     * If the item is successfully added to the inventory, and if the item is a "GameEnd" item
     * and the inventory contains "Boots", it sets the flag to end the game. Additionally, if the
     * picked up item is "Boots", it increases the player's speed by the speed boost provided by the boots.
     *
     * @param item The item to pick up.
     * @return True if the item is successfully picked up and added to the inventory, false otherwise.
     */
    public boolean pickUpItem(Item item) {
        if (item.getName().equals("GameEnd") && inventoryModel.contains("Boots")){
            endTheGame = true;
        }
        if (inventoryModel.addItem(item)){
            if (item.getName().equals("Boots")){
                speed += ((Boots) item).getSpeedBoost();
            }
            return true;
        }
        return false;
    }

    public int getScreenX() {
        return screenX;
    }

    private Item createItemInstance(String className) {
        switch (className){
            case "BagPart2":
                return new BagPart2();
            case "BagPart1":
                return new BagPart1();
            case "KeyPart1":
                return new KeyPart1();
            case "KeyPart2":
                return new KeyPart2();
            case "KeyPart3":
                return new KeyPart3();
            case "Key":
                return new KeyDoor();
        }
        return null;
    }

    public int getScreenY() {
        return screenY;
    }

    public InventoryModel getInventoryModel() {
        return inventoryModel;
    }

    public TileType getInteractedWithTile1() {
        return interactedWithTile1;
    }

    public void setInteractedWithTile1(TileType interactedWithTile1) {
        this.interactedWithTile1 = interactedWithTile1;
    }

    public TileType getInteractedWithTile2() {
        return interactedWithTile2;
    }

    public void setInteractedWithTile2(TileType interactedWithTile2) {
        this.interactedWithTile2 = interactedWithTile2;
    }

    public boolean deleteItem(String itemName) {
        return inventoryModel.deleteItem(itemName);
    }

    public void setInventoryModel(InventoryModel inventoryModel) {
        this.inventoryModel = inventoryModel;
    }



}
