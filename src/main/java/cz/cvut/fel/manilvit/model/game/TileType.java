package cz.cvut.fel.manilvit.model.game;

public enum TileType {

    GRASS,
    WALL,
    WATER,
    EARTH,
    TREE,
    SAND,
    DOOR;

    private boolean isPassable;
    private boolean isInteractive;


    public boolean isPassable() {
        return isPassable;
    }

    public void setIsPassable(boolean passable) {
        isPassable = passable;
    }


    public boolean isInteractive() {
        return isInteractive;
    }

    public void setInteractive(boolean interactive) {
        isInteractive = interactive;
    }



}
