package cz.cvut.fel.manilvit.model.items;

public class Item {

    String name;
    int tileNumX = -1;
    int tileNumY = -1;
    boolean addedToCraft;

    public Item(){

    }


    public Item (int tileNumX, int tileNumY){
        this.tileNumX = tileNumX;
        this.tileNumY = tileNumY;
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTileNumX() {
        return tileNumX;
    }

    public void setTileNumX(int tileNumX) {
        this.tileNumX = tileNumX;
    }

    public int getTileNumY() {
        return tileNumY;
    }

    public void setTileNumY(int tileNumY) {
        this.tileNumY = tileNumY;
    }


    public boolean isAddedToCraft() {
        return addedToCraft;
    }

    public void setAddedToCraft(boolean addedToCraft) {
        this.addedToCraft = addedToCraft;
    }
}
