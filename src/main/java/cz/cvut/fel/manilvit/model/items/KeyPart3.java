package cz.cvut.fel.manilvit.model.items;

public class KeyPart3 extends Item{
    public KeyPart3(){
        name = "KeyPart3";
    }

    public KeyPart3(int tileNumX, int tileNumY){
        super(tileNumX, tileNumY);
        name = "KeyPart3";
    }
}
