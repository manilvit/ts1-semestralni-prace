package cz.cvut.fel.manilvit.model.items;

public class Key extends Item {

    public Key(){
        name = "Key";
    }

    public Key(int tileNumX, int tileNumY){
        super(tileNumX, tileNumY);
        name = "Key";
    }
}
