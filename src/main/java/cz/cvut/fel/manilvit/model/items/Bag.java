package cz.cvut.fel.manilvit.model.items;

public class Bag extends Item{
    public Bag(){
        name = "Bag";
    }

    public Bag(int tileNumX, int tileNumY){
        super(tileNumX, tileNumY);
        name = "Bag";
    }




}
