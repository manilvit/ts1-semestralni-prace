package cz.cvut.fel.manilvit.model.items;

public class KeyDoor extends Item {

    public KeyDoor(){
        name = "KeyDoor";
    }

    public KeyDoor(int tileNumX, int tileNumY){
        super(tileNumX, tileNumY);
        name = "KeyDoor";
    }
}
