package cz.cvut.fel.manilvit.model.items;

public class KeyPart1 extends Item{
    public KeyPart1(){
        name = "KeyPart1";
    }

    public KeyPart1(int tileNumX, int tileNumY){
        super(tileNumX, tileNumY);
        name = "KeyPart1";
    }
}
