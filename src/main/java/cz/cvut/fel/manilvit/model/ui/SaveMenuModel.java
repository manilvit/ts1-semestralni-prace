package cz.cvut.fel.manilvit.model.ui;

import cz.cvut.fel.manilvit.controller.GameState;
import cz.cvut.fel.manilvit.controller.input.KeyInputType;

public class SaveMenuModel extends MenuModel{
    public SaveMenuModel() {
        this.currentIdx = 0;
        this.menuItemNames = new String[2];
        this.menuItemNames[0] = "Continue";
        this.menuItemNames[1] = "Save & Exit";
    }

    private GameState executeItem(int currentIdx) {
        GameState gameState = null;
        switch (currentIdx) {
            case 0:
                gameState = GameState.RUN;
                break;
            case 1:
                gameState = GameState.SAVE;
                break;
        }
        return gameState;
    }


    public GameState move(KeyInputType keyInputType) {
        GameState gameState = GameState.SAVE_MENU;
        switch (keyInputType) {
            case ENTER:
                gameState = executeItem(currentIdx);
                System.out.println(gameState);
                break;
            case DOWN:
                currentIdx += 1;
                currentIdx %= 2;
                break;
            case UP:
                currentIdx -= 1;
                if (currentIdx == -1) {
                    currentIdx = 1;
                }
                break;
        }
        return gameState;
    }
}
