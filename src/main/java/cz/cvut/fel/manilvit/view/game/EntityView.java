package cz.cvut.fel.manilvit.view.game;

import java.awt.image.BufferedImage;

public class EntityView {

    protected BufferedImage lastImage;
    protected BufferedImage[] upSprites;
    protected BufferedImage[] downSprites;
    protected BufferedImage[] leftSprites;
    protected BufferedImage[] rightSprites;
    protected int spriteCounter = 0;
    protected int spriteNum = 1;


    public void incrementSpriteCounter() {
        spriteCounter++;
    }

    public void resetSpriteCounter() {
        spriteCounter = 0;
    }

    public void toggleSprite() {
        spriteNum = (spriteNum == 1) ? 2 : 1;
    }


    public int getSpriteCounter() {
        return spriteCounter;
    }
}
