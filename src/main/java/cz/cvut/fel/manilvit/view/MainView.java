package cz.cvut.fel.manilvit.view;

import cz.cvut.fel.manilvit.Configuration;
import cz.cvut.fel.manilvit.controller.GameState;
import cz.cvut.fel.manilvit.controller.input.KeyHandler;
import cz.cvut.fel.manilvit.model.game.GameMapModel;
import cz.cvut.fel.manilvit.model.game.PlayerModel;
import cz.cvut.fel.manilvit.model.ui.MainMenuModel;
import cz.cvut.fel.manilvit.model.ui.SaveMenuModel;
import cz.cvut.fel.manilvit.view.game.GameMapView;
import cz.cvut.fel.manilvit.view.game.PlayerView;
import cz.cvut.fel.manilvit.view.ui.UiView;

import javax.swing.*;
import java.awt.*;
import java.util.TimerTask;

public class MainView extends JPanel {


    private Configuration configuration;
    private GameState gameState;

    private MainMenuModel mainMenuModel;
    private SaveMenuModel saveMenuModel;

    private PlayerModel playerModel;
    private PlayerView playerView;

    private GameMapModel gameMapModel;
    private GameMapView gameMapView;

    private UiView uiView;
    private boolean enough = false;


    public MainView(KeyHandler keyHandler, Configuration configuration, PlayerView playerView, GameMapView gameMapView) {
        this.setPreferredSize(new Dimension(configuration.screenWidth, configuration.screenHeight));
        this.setBackground(Color.black);
        this.setDoubleBuffered(true); // It is something for better performance
        this.addKeyListener(keyHandler);
        this.setFocusable(true); //using this stuff game panel can be "focused" to receive key input

        this.configuration = configuration;

        this.playerView = playerView;
        this.gameMapView = gameMapView;
        this.uiView = new UiView();

    }

    /**
     * Updates the graphics (GFX) based on the current game state and models.
     * This method updates the graphics by setting the current game state and models, including the main menu,
     * save menu, player, and game map models. After updating the graphics, it triggers a repaint of the
     * component to reflect the changes visually.
     *
     * @param gameState    The current state of the game.
     * @param mainMenuModel The model for the main menu.
     * @param saveMenuModel The model for the save menu.
     * @param playerModel  The model for the player character.
     * @param gameMapModel The model for the game map.
     */
    public void updateGFX(GameState gameState, MainMenuModel mainMenuModel, SaveMenuModel saveMenuModel, PlayerModel playerModel, GameMapModel gameMapModel) {

        this.mainMenuModel = mainMenuModel;
        this.saveMenuModel = saveMenuModel;
        this.gameMapModel = gameMapModel;
        this.playerModel = playerModel;
        this.gameState = gameState;

        repaint();

    }


    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (gameState == GameState.END){
//            LOG.info("Game Finish!");
        }


        if (gameState == GameState.MAIN_MENU && mainMenuModel != null) {
            uiView.drawMenu(g, mainMenuModel, configuration.tileSize);
        }


        if (gameState == GameState.RUN || gameState == GameState.SAVE_MENU || gameState == GameState.CRAFT) {


            if (gameMapModel != null) {
                gameMapView.draw(g, gameMapModel, playerModel, configuration.tileSize);
            }

            if (playerModel != null) {
                playerView.draw(g, playerModel, configuration.tileSize);
                uiView.drawInventory(g, playerModel.getInventoryModel(), gameMapView.getItemImages(), configuration.tileSize);

                if (gameState == GameState.CRAFT) {
                    uiView.drawCraftWindow(g, playerModel.getInventoryModel(), gameMapView.getItemImages(), configuration.tileSize);
                }
            }

            if (gameState == GameState.SAVE_MENU && saveMenuModel != null) {
                uiView.drawMenu(g, saveMenuModel, configuration.tileSize);
            }
        }

        g.dispose();
    }


}
