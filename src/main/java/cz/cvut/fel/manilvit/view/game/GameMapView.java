package cz.cvut.fel.manilvit.view.game;

import cz.cvut.fel.manilvit.controller.MainController;
import cz.cvut.fel.manilvit.model.game.GameMapModel;
import cz.cvut.fel.manilvit.model.game.PlayerModel;
import cz.cvut.fel.manilvit.model.game.TileType;
import cz.cvut.fel.manilvit.model.items.Item;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class GameMapView {


    private final static Logger LOG = Logger.getLogger(MainController.class.getName());

    private Map<TileType, BufferedImage> tileImages;
    private Map<String, BufferedImage> itemImages;


    public GameMapView(List<TileType> tiles, List<String> itemNames){
        tileImages = new HashMap<>();
        itemImages = new HashMap<>();
        loadTileImages(tiles);
        loadItemImages(itemNames);
    }

    /**
     * Loads tile images into memory based on the provided list of tile types.
     * This method iterates through the list of tile types and attempts to load the corresponding
     * image for each tile from the "/tiles/" directory. The loaded images are stored in a map
     * associating each tile type with its respective image.
     *
     * @param tiles The list of tile types for which images need to be loaded.
     */
    private void loadTileImages(List<TileType> tiles) {
        for (TileType tile : tiles) {
            try {
                BufferedImage image = ImageIO.read(getClass().getResourceAsStream("/tiles/" + tile.name().toLowerCase() + ".png"));
                tileImages.put(tile, image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    /**
     * Loads item images into memory based on the provided list of item names.
     * This method iterates through the list of item names and attempts to load the corresponding
     * image for each item from the "/items/" directory. The loaded images are stored in a map
     * associating each item name with its respective image. The "GameEnd" item is excluded from loading.
     *
     * @param itemNames The list of item names for which images need to be loaded.
     */
    private void loadItemImages(List<String> itemNames) {
        for (String itemName : itemNames){
            if (itemName.equals("GameEnd")){
                continue;
            }

            try {
                BufferedImage image = ImageIO.read(getClass().getResourceAsStream("/items/" + itemName.toLowerCase() + ".png"));
                itemImages.put(itemName, image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        LOG.info("Loading item images done");
    }

    /**
     * Draws a tile on the graphics context at the specified coordinates.
     * This method retrieves the image corresponding to the provided tile type from the tile images map.
     * If an image is found, it is drawn on the graphics context at the specified coordinates with the given tile size.
     *
     * @param g         The graphics context on which to draw the tile.
     * @param tile      The type of the tile to draw.
     * @param x         The x-coordinate of the top-left corner of the tile.
     * @param y         The y-coordinate of the top-left corner of the tile.
     * @param tileSize  The size of the tile to draw.
     */
    private void drawTile(Graphics g, TileType tile, int x, int y, int tileSize) {
        BufferedImage image = tileImages.get(tile);
        if (image != null) {
            g.drawImage(image, x, y, tileSize, tileSize, null);
        } else {
//            System.out.println(tile);
        }
    }


    /**
     * Draws an item on the graphics context at the specified coordinates.
     * This method retrieves the image corresponding to the provided item name from the item images map.
     * If an image is found, it is drawn on the graphics context at the specified coordinates with the given tile size.
     *
     * @param g         The graphics context on which to draw the item.
     * @param itemName  The name of the item to draw.
     * @param x         The x-coordinate of the top-left corner of the item.
     * @param y         The y-coordinate of the top-left corner of the item.
     * @param tileSize  The size of the item to draw.
     */
    private void drawItem(Graphics g, String itemName, int x, int y, int tileSize) {
        BufferedImage image = itemImages.get(itemName);
        if (image != null) {
            g.drawImage(image, x, y, tileSize, tileSize, null);
        } else {
//            System.out.println("sdfs" + itemName);
        }
    }

    /**
     * Draws the game map and player on the graphics context.
     * This method renders the game map, player, and items on the screen based on their positions and the size of tiles.
     *
     * @param g            The graphics context to draw on.
     * @param gameMapModel The game map model containing tile information.
     * @param playerModel  The player model containing player information.
     * @param tileSize     The size of each tile in pixels.
     */
    public void draw(Graphics g, GameMapModel gameMapModel, PlayerModel playerModel, int tileSize) {
        // Get player world coordinates and screen coordinates
        int playerWorldX = playerModel.getWorldX();
        int playerWorldY = playerModel.getWorldY();
        int playerScreenX = playerModel.getScreenX();
        int playerScreenY = playerModel.getScreenY();

        // Get maximum tile numbers and map tiles
        int maxTileNumX = gameMapModel.getMaxTileNumX();
        int maxTileNumY = gameMapModel.getMaxTileNumY();
        int[][] tiles = gameMapModel.getMap();

        // Initialize variables for tile iteration
        int tileNumX = 0;
        int tileNumY = 0;

        // Iterate through all tiles in the map
        while (tileNumY < maxTileNumY) {
            // Get the type of tile to draw
            TileType tile = TileType.values()[tiles[tileNumY][tileNumX]];

            // Calculate world coordinates and screen coordinates of the current tile
            int worldX = tileNumX * tileSize;
            int worldY = tileNumY * tileSize;
            int screenX = worldX - playerWorldX + playerScreenX;
            int screenY = worldY - playerWorldY + playerScreenY;

            // Draw the tile only if it is within the screen boundaries
            if (worldX + tileSize > playerWorldX - playerScreenX
                    && worldX - tileSize < playerWorldX + playerScreenX
                    && worldY + tileSize > playerWorldY - playerScreenY
                    && worldY - tileSize < playerWorldY + playerScreenY) {
                // Draw the tile
                drawTile(g, tile, screenX, screenY, tileSize);

                // Check if there is an item on the current tile and draw it
                Item item = gameMapModel.getItemByTileNum(tileNumY, tileNumX);
                if (item != null) {
                    drawItem(g, item.getName(), screenX, screenY, tileSize);
                }
            }

            // Move to the next tile
            tileNumX++;

            // Reset tileNumX and move to the next row if reached the end of the row
            if (tileNumX == maxTileNumX) {
                tileNumX = 0;
                tileNumY++;
            }
        }
    }

    public Map<String, BufferedImage> getItemImages() {
        return itemImages;
    }
}
