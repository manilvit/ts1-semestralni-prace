package cz.cvut.fel.manilvit.view.ui;

import cz.cvut.fel.manilvit.model.game.InventoryModel;
import cz.cvut.fel.manilvit.model.items.Item;
import cz.cvut.fel.manilvit.model.ui.MainMenuModel;
import cz.cvut.fel.manilvit.model.ui.MenuModel;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Map;

public class UiView {


    public void drawMenu(Graphics g, MenuModel menuModel, int tileSize){
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 768, 576); // Очистка фона
        g.setColor(Color.WHITE);
        g.setFont(new Font("Arial", Font.BOLD, 24));

        String[] menuItemNames = menuModel.getMenuItemNames();
        int currentIdx = menuModel.getCurrentIdx();

        for (int i = 0; i < menuItemNames.length; i++) {
            if (i == currentIdx) {
                g.setColor(Color.YELLOW);
            } else {
                g.setColor(Color.WHITE);
            }
            g.drawString(menuItemNames[i], 350, 200 + i * 30); // Отрисовка пунктов меню
        }

    }


    private void drawItem(Graphics g, int x, int y,  BufferedImage image, int tileSize){
        g.drawImage(image, x, y, tileSize, tileSize, null);
    }
    public void drawInventory(Graphics g, InventoryModel inventoryModel, Map<String, BufferedImage> itemImages, int tileSize) {
        int x = tileSize * 2;
        int y = tileSize * 10;

        Item [] items = inventoryModel.getItems();
        int i = 0;

        for (Item item : items){
            if (i >= inventoryModel.getSize()){
                return;
            }
            int borderSize = 2;


            if (i == inventoryModel.getCurrentIdx()){
                g.setColor(Color.GREEN);
            } else {
                g.setColor(Color.BLACK);
            }

            g.fillRect(x - borderSize, y - borderSize, tileSize + 2 * borderSize, tileSize + 2 * borderSize);



            g.setColor(new Color(255, 255, 255)); // Бежевый цвет
            g.fillRect(x, y, tileSize, tileSize);

            if (item != null){
                if (item.isAddedToCraft()){
                    g.setColor(Color.RED); // Бежевый цвет
                    g.fillRect(x, y, tileSize, tileSize);
                }
                drawItem(g, x, y, itemImages.get(item.getName()), tileSize);
            }



            x += tileSize;
            i++;
        }
    }


    public void drawCraftWindow(Graphics g, InventoryModel inventoryModel, Map<String, BufferedImage> itemImages, int tileSize){
        int x = tileSize * 4;
        int y = tileSize * 5;



        Item [] craft = inventoryModel.getCraft();

        for (int i = 0; i < craft.length; i++){
            int borderSize = 2;

            g.setColor(Color.BLACK);

            g.fillRect(x - borderSize, y - borderSize, tileSize + 2 * borderSize, tileSize + 2 * borderSize);

            g.setColor(new Color(255, 255, 255)); // Бежевый цвет
            g.fillRect(x, y, tileSize, tileSize);

            if (craft[i] != null){
                drawItem(g, x, y, itemImages.get(craft[i].getName()), tileSize);
            }



            x += tileSize;
        }
    }
}
