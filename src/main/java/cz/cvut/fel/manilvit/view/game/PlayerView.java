package cz.cvut.fel.manilvit.view.game;

import cz.cvut.fel.manilvit.controller.input.KeyInputType;
import cz.cvut.fel.manilvit.model.game.PlayerModel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class PlayerView extends EntityView{

    public PlayerView(){
        getPlayerImage();
        lastImage = downSprites[0];
    }


    /**
     * Loads sprite images from the provided paths and returns them as an array of BufferedImages.
     * This method loads sprite images from the specified paths and returns them as an array of BufferedImages.
     * Each path corresponds to a single sprite image. If any of the images fail to load, an IOException is caught
     * and the stack trace is printed, but the method continues loading the remaining images.
     *
     * @param paths The paths to the sprite images to load.
     * @return An array of BufferedImages containing the loaded sprite images.
     */
    private BufferedImage[] loadSprites(String... paths) {
        BufferedImage[] sprites = new BufferedImage[paths.length];
        try {
            for (int i = 0; i < paths.length; i++) {
                sprites[i] = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(paths[i])));
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
        return sprites;
    }

    /**
     * Loads player sprite images for different directions.
     * This method loads sprite images for the player character facing up, down, left, and right directions.
     * It calls the loadSprites method with the paths to the respective sprite images for each direction.
     */
    private void getPlayerImage(){
        upSprites = loadSprites("/player/boy_up_1.png", "/player/boy_up_2.png");
        downSprites = loadSprites("/player/boy_down_1.png", "/player/boy_down_2.png");
        leftSprites = loadSprites("/player/boy_left_1.png", "/player/boy_left_2.png");
        rightSprites = loadSprites("/player/boy_right_1.png", "/player/boy_right_2.png");
    }


    /**
     * Draws the player sprite on the graphics context.
     * This method renders the player sprite on the screen based on its position and direction.
     *
     * @param g           The graphics context to draw on.
     * @param playerModel The player model containing player information.
     * @param tileSize    The size of the player sprite in pixels.
     */
    public void draw(Graphics g, PlayerModel playerModel, int tileSize) {
        // Set the color for drawing the player sprite
        g.setColor(Color.WHITE);

        // Get the screen coordinates and direction of the player
        int screenX = playerModel.getScreenX();
        int screenY = playerModel.getScreenY();
        KeyInputType keyInputType = playerModel.getDirection();

        // Initialize a BufferedImage variable to store the player sprite image
        BufferedImage image = null;

        // Determine the appropriate player sprite image based on the direction
        switch (keyInputType) {
            case UP:
                image = upSprites[spriteNum - 1];
                break;
            case DOWN:
                image = downSprites[spriteNum - 1];
                break;
            case LEFT:
                image = leftSprites[spriteNum - 1];
                break;
            case RIGHT:
                image = rightSprites[spriteNum - 1];
                break;
        }

        // Use the last image if the current image is null
        if (image == null) {
            image = lastImage;
        } else {
            // Store the current image as the last image
            lastImage = image;
        }

        // Draw the player sprite on the graphics context
        g.drawImage(image, screenX, screenY, tileSize, tileSize, null);
    }
}
